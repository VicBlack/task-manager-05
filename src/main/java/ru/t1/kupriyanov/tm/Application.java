package ru.t1.kupriyanov.tm;

import static ru.t1.kupriyanov.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    private static void processArguments(String[] args) {
        if (args == null || args.length == 0) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    private static void processArgument(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case HELP:
                showHelp();
                break;
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
        }
    }

    private static void showError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported.");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show list arguments.\n", HELP);
        System.out.printf("%s - Show info about programmer.\n", ABOUT);
        System.out.printf("%s - Show version.\n", VERSION);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Anton Kupriyanov");
        System.out.println("E-mail: ankupr29@gmail.com");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.1");
    }

}
